/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo
 */
public class ComandParse {
    public static int[] toAttack(String command){
        String[] cmds = command.split(" ");
        cmds = cmds[1].split(",");
        int[] resps = new int[cmds.length];
        for (int i = 0; i < resps.length; i++) {
            resps[i] = Integer.parseInt(cmds[i]);
        }
        return resps;
    }
    
    public static List<Point> toPositions(String command){
        List<Point> positions = new ArrayList<>();
        String[] strPosicoes = command.split("\n");
        //strPosicoes = strPosicoes[1].split("\n");
        for (int i = 1; i < strPosicoes.length; i++) {
            String[] pos = strPosicoes[i].split(",");
            int x = Integer.parseInt(pos[0]);
            int y = Integer.parseInt(pos[1]);
            positions.add(new Point(x, y));
            System.out.println("_|_" + positions.get(i-1));
        }
        return positions;
    }
    
    public static String toLevel(List<Point> players, List<Point> obstacles){
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        for (int i = 0; i < players.size(); i++) {
            Point jogador = players.get(i);
            System.out.println("_|_" + jogador.toString());
            sb.append(jogador.x).append(",").append(jogador.y);
            if (i < players.size() - 1)
                sb.append("\n");
        }
        
        if (obstacles.size() > 0)
            sb.append("\n");
        
        for (int i = 0; i < obstacles.size(); i++) {
            Point obs = obstacles.get(i);
            System.out.println("_|_" + obs.toString());
            sb.append(obs.x).append(",").append(obs.y);
            if (i < obstacles.size() - 1)
                sb.append("\n");
        }
        
        return sb.toString();
    }
}
