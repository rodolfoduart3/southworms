package model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

import javax.swing.ImageIcon;

import animator.Animator;
import animator.SouthParkAnimator;

public class Jogador extends Elemento {

    private Graphics2D graphics;
    private Image projetilImage;
    private int indice;
    private int lifePlayer;
    private String namePlayer;

    public Jogador() {
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        ImageIcon referencia = new ImageIcon(s + "/images/kenny/KennyAndandoGif.gif");
        elementoImage = referencia.getImage();
        nome = "player";
        x1 = 120;
        y1 = 420;
        largura = altura = 35;
        x2 = x1 + largura;
        y2 = y1 + altura;
        lifePlayer = 100;
        this.indice = 0;
        animator = new SouthParkAnimator(this);
        this.setIsPlayer(true);
    }

    public Jogador(int x1, int y1, int largura, int altura, String nome) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x1 + largura;
        this.y2 = y1 + altura;
        this.largura = largura;
        this.altura = altura;
        this.lifePlayer = 100;
        animator = new SouthParkAnimator(this);
        this.setIsPlayer(true);
    }

    public Jogador(ImageIcon referencia) {
        elementoImage = referencia.getImage();
        nome = "player";
        x1 = 120;
        y1 = 420;
        largura = elementoImage.getWidth(null);
        altura = elementoImage.getHeight(null);
        x2 = x1 + largura;
        y2 = y1 + altura;
        lifePlayer = 100;
        animator = new SouthParkAnimator(this);
        this.setIsPlayer(true);
    }

    public void onRender(Graphics2D g) {
        this.setGraphics(g);

        animator.update();

        g.setColor(Color.WHITE);
        g.drawImage(elementoImage, x1, y1, null);
        g.setColor(Color.RED);
        g.drawString(Integer.toString(getLifePlayer()), x1 + 20, y1);
    }

    public void mover(int larguraTela, int alturaTela,
            HashMap<Integer, Boolean> keyPool, WormsGame worms) {
        if (worms.checaTempo() < 150) {
            return;
        }

        // tratar colis�o com as bordas laterais
        if (x1 + largura >= larguraTela) { // borda da direita somar a largura
            // do sprite
            setX1(x1 - FORCE);
        } else {
            if (x1 <= 0) {// borda da esquerda
                setX1(getX1() + FORCE);
            } else {

                // applyForce();
                if (keyPool.get(KeyEvent.VK_LEFT) != null) {
                    //System.out.println("saida: " + worms.getMessage().getText());
                    System.out.println("esquerda");
                    worms.attTempo();
                    moverEsquerda(worms, true);
                }

                if (keyPool.get(KeyEvent.VK_RIGHT) != null) {
                    //System.out.println("saida: " + worms.getMessage().getText());
                    System.out.println("direita");
                    worms.attTempo();
                    moverDireita(worms, true);
                }

            }
        }

        // tratar colis�o com as bordas superiores e inferiores
        if (getY1() + getAltura() >= alturaTela) {// borda inferior somar a
            // altura do sprite
            setY1(getY1() - FORCE);
        } else {

            if (getY1() - getAltura() <= 0) { // borda superior
                setY1(getY1() + FORCE);
            }

            /*else {

             if (keyPool.get(KeyEvent.VK_UP) != null) {
             //System.out.println("saida: " + worms.getMessage().getText());
             worms.attTempo();
             //moverCima(worms, true);
             }

             if (keyPool.get(KeyEvent.VK_DOWN) != null) {
             //System.out.println("saida: " + worms.getMessage().getText());
             worms.attTempo();
             //moverBaixo(worms, true);
             }
             }*/
        }

        if (keyPool.get(KeyEvent.VK_ESCAPE) != null) {//mover para baixo
            worms.enviarComando("sair");
            //worms.disconnected();

        }
    }

    public Projetil atirar(int x, int y, WormsGame worms) {

        Projetil p = new Projetil(getX1(), getY1() - altura - 30, x, y, 50, 50);
        //p.setProjetilImage(this.getProjetilImage());
        System.out.println(this.getProjetilImage());
        System.out.println(this.getElementoImage());
        p.setProjetilImage(this.getProjetilImage());
        //p.setNome(this.getNome());
        if (worms != null) {
            String comando = "atirar " + this.getIndice() + "," + x + "," + y;
            worms.enviarComando(comando);
        }
        return p;
    }

    public void moverBaixo(WormsGame worms, boolean sendMessage) {
        //mover para baixo
        if (sendMessage) {
            worms.enviarComando("baixo");
        }
        setY1(getY1() + FORCE);
        setUltimoMovimento(BAIXO);
    }

    public void moverCima(WormsGame worms, boolean sendMessage) {
        //mover para cima VK_W
        if (sendMessage) {
            worms.enviarComando("cima");
        }
        setY1(getY1() - FORCE);
        setUltimoMovimento(CIMA);
    }

    public void moverDireita(WormsGame worms, boolean sendMessage) {
        //System.out.println("entrou no moverdir");
        //mover para direita
        if (sendMessage) {
            //System.out.println("mover com true");
            worms.enviarComando(this.getIndice() + ",direita");
        }
        setX1(getX1() + FORCE);
        setUltimoMovimento(DIREITA);
    }

    public void moverEsquerda(WormsGame worms, boolean sendMessage) {
        //mover para esquerda
        if (sendMessage) {
            worms.enviarComando(this.getIndice() + ",esquerda");
        }
        setX1(getX1() - FORCE);
        setUltimoMovimento(ESQUERDA);
    }

    public void sofreuDano(WormsGame worms, boolean sendMessage, int dano) {
        if (sendMessage) {
            worms.enviarComando(this.getIndice() + ",dano");
        }
        this.setLifePlayer(getLifePlayer() - dano);
    }

    public Graphics2D getGraphics() {
        return graphics;
    }

    public void setGraphics(Graphics2D graphics) {
        this.graphics = graphics;
    }

    public Image getProjetilImage() {
        return projetilImage;
    }

    public void setProjetilImage(Image projetilImage) {
        this.projetilImage = projetilImage;
    }

    public int getLifePlayer() {
        return lifePlayer;
    }

    public void setLifePlayer(int lifePlayer) {
        this.lifePlayer = lifePlayer;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    public String getNamePlayer() {
        return namePlayer;
    }

    public void setNamePlayer(String namePlayer) {
        this.namePlayer = namePlayer;
    }

    public Animator getAnimator() {
        return animator;
    }

    public void setAnimator(Animator animator) {
        this.animator = animator;
    }
}
