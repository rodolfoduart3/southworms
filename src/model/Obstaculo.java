/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import animator.Animator;
import animator.SouthParkAnimator;
import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author Luis
 */
public class Obstaculo extends Elemento {
    
    
    public Obstaculo(int x1, int y1, int tamanho) {
        this(x1, y1, tamanho, tamanho);
    }
    
    public Obstaculo(int x1, int y1, int largura, int altura) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x1 + largura;
        this.y2 = y1 + altura;
        this.largura = largura;
        this.altura = altura;
        this.setIsPlayer(false);
        animator = new SouthParkAnimator(this);
        
        Personagens p = new Personagens();
        animator.addAnimation(SouthParkAnimator.IDLE, p.getListaBox().get(0).getImage());
    }
    
    @Override
    public void onRender(Graphics2D g) {
        animator.update();
        g.drawImage(elementoImage, x1, y1, null);
        //g.setColor(Color.GREEN);
        //g.fillRect(x1, y1, largura, altura);
    }
}
