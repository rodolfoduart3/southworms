package model;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Personagens {

    List<ImageIcon> listaSkins;
    List<ImageIcon> listaProjeteis;
    List<ImageIcon> listaAtaques;
    List<ImageIcon> listaHits;
    List<ImageIcon> listaBox;

    public Personagens() {
        listaSkins = new ArrayList<>();
        listaProjeteis = new ArrayList<>();
        listaAtaques = new ArrayList<>();
        listaHits = new ArrayList<>();
        listaBox = new  ArrayList<>();
        
        carregarProjeteis();
        carregarSkins();
        carregarAtaques();
        carregarHits();
        carregarBox();
    }
    
    private void carregarBox() {
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        listaBox.add(new ImageIcon(s + "/images/box/CaixaSouthPark.png"));
        listaBox.add(new ImageIcon(s + "/images/box/CaixaSouthPark2.png"));
        listaBox.add(new ImageIcon(s + "/images/box/CaixaSouthPark.gif"));
    }

    private void carregarSkins() {
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();

        listaSkins.add(new ImageIcon(s + "/images/kenny/KennyAndandoGif.gif"));
        listaSkins.add(new ImageIcon(s + "/images/cartman/CartmanAndandoGif.gif"));
        listaSkins.add(new ImageIcon(s + "/images/kyle/KyleAndandoGif.gif"));
        listaSkins.add(new ImageIcon(s + "/images/stan/StanAndandoGif.gif"));
    }
    
    private void carregarProjeteis(){
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        listaProjeteis.add(new ImageIcon(s + "/images/projeteis/KENNY.gif"));
        listaProjeteis.add(new ImageIcon(s + "/images/projeteis/CARTMAN.gif"));
        listaProjeteis.add(new ImageIcon(s + "/images/projeteis/Kyle.png"));
        listaProjeteis.add(new ImageIcon(s + "/images/projeteis/Stan.png"));
    }
    
    private void carregarAtaques(){
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();

        listaAtaques.add(new ImageIcon(s + "/images/kenny/KennyAtacando.gif"));
        listaAtaques.add(new ImageIcon(s + "/images/cartman/CartmanAtaque.gif"));
        listaAtaques.add(new ImageIcon(s + "/images/kyle/KyleAtaque.gif"));
        listaAtaques.add(new ImageIcon(s + "/images/stan/StanAtaque.gif"));
    }
    
    private void carregarHits(){
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();

        listaHits.add(new ImageIcon(s + "/images/kenny/KennyDanoGif.gif"));
        listaHits.add(new ImageIcon(s + "/images/cartman/CartmanDano.gif"));
        listaHits.add(new ImageIcon(s + "/images/kyle/KyleDano.gif"));
        listaHits.add(new ImageIcon(s + "/images/stan/StanDano.gif"));
    }
    
    public List<ImageIcon> getListaBox() {
        return listaBox;
    }

    public List<ImageIcon> getListaSkins() {
        return listaSkins;
    }
    
    public List<ImageIcon> getListaProjeteis() {
        return listaProjeteis;
    }
    
    public List<ImageIcon> getListaAtaques() {
        return listaAtaques;
    }
    
    public List<ImageIcon> getListaHits() {
        return listaHits;
    }
}