package model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;

public class Projetil extends Elemento {

    private int xMax, yMax; // X/Y maximo atingido pelo projetil
    private int movHorizontal, movVertical;
    private boolean subindo = true;
    private Image projetilImage;

    public Projetil(int x1, int y1, int xm, int ym, int largura, int altura) {
        super();
        nome = "";

        this.x1 = x1;
        this.y1 = y1;

        this.x2 = x1 + largura;
        this.y2 = y2 + altura;
        this.largura = largura;
        this.altura = altura;

        xMax = xm;
        yMax = ym;

        //define de qual dire��o o projetil vem
        if (this.x1 > xMax) {
            movHorizontal = ESQUERDA;
        } else {
            movHorizontal = DIREITA;
        }

        if (this.y1 > yMax) {
            movVertical = CIMA;

        } else {
            movVertical = BAIXO;
        }

    }

    public void onRender(Graphics2D g, int distancia) {
        // verifica para qual lado o projetil ir� seguir
        if (movHorizontal == ESQUERDA) {
            x1 -= distancia;
        } else {
            x1 += distancia;
        }

        if (movVertical == CIMA) {
            if (y1 > yMax && subindo) { // indica o momento em que o projetil
                // chegou a sua altura maxima e ira cair
                y1 -= distancia;
            } else {
                subindo = false;
                y1 += distancia;
            }
        } else {
            y1 += distancia;
        }

        x2 = x1 + largura;
        y2 = y1 + altura;

        // (re)desenha o projetil
        g.setColor(Color.red);
        //g.fillRect(x1, y1, largura, altura);
        //System.out.println("projetil image???" + projetilImage);
        g.drawImage(projetilImage, x1, y1,null);

    }

    public int getxMax() {
        return xMax;
    }

    public void setxMax(int xMax) {
        this.xMax = xMax;
    }

    public int getyMax() {
        return yMax;
    }

    public void setyMax(int yMax) {
        this.yMax = yMax;
    }

    public Image getProjetilImage() {
        return projetilImage;
    }

    public void setProjetilImage(Image projetilImage) {
        this.projetilImage = projetilImage;
    }

}
