package model;

import animator.SouthParkAnimator;
import bean.ChatMessage;
import bean.ChatMessage.Action;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import core.Game;
import frame.ClienteFrame;
import java.awt.Color;
import java.awt.Point;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.JOptionPane;
import level.generation.SouthWormsLG;

import service.ClienteService;

public class WormsGame extends Game implements KeyListener, MouseListener {

    public static final String DIREITA = "direita";
    public static final String ESQUERDA = "esquerda";
    public static final String ATIRAR = "atirar";
    public static final String DANO = "dano";
    public static final int MAX_PLAYERS = 2;

    private static long tempoAntes = 0;
    private static long tempoAtual = 0;
    private static final int FORCE = 10;
    private static final String MSG_TURNO = " es tua vez";
    //private static final Random Gerador = new Random();
    public static String txtMensagem = "";

    private Map<Integer, Jogador> listaJogadores = new HashMap<Integer, Jogador>();
    private Personagens listaPersonagens = new Personagens();

    private Jogador jogador;
    private FaseA fase;
    private HashMap<Integer, Boolean> keyPool;
    private String nomePlayer;

    List<Projetil> projeteis, projeteis_colididos;
    List<Elemento> elementos_jogo, elementos_colididos;
    Graphics2D graphics;

    private ClienteFrame clienteChat;
    private Socket socket;
    private ChatMessage message;
    private ClienteService service;
    private int onlines = 0;
    private boolean abriu;
    private int indiceJogadorAtual = 0;
    private int playerCounter = 0;
    Set<String> names = new HashSet<>();

    public WormsGame() {
        //Path currentRelativePath = Paths.get("");
        //String s = currentRelativePath.toAbsolutePath().toString();
        //JOptionPane.showMessageDialog(null, s);
        //carregarJogadores();
        abriu = false;
        nomePlayer = JOptionPane.showInputDialog("Insira seu nome");
        conectar();
        jogador = new Jogador();
    }

    private class ListenerSocket implements Runnable {

        private ObjectInputStream input;

        public ListenerSocket(Socket socket) {
            try {
                this.input = new ObjectInputStream(socket.getInputStream());
            } catch (IOException ex) {
                Logger.getLogger(ClienteFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        public void run() {
            ChatMessage message = null;
            try {
                while ((message = (ChatMessage) input.readObject()) != null) {
                    ChatMessage.Action action = message.getAction();
                    if (action.equals(ChatMessage.Action.CONNECT)) {
                        //System.out.println("connect action");
                        connected(message);
                    } else if (action.equals(ChatMessage.Action.DISCONNECT)) {
                        disconnected();
                        socket.close();
                        System.exit(0);
                    } else if (action.equals(ChatMessage.Action.SEND_ONE)) {
                        receive(message);
                    } else if (action.equals(ChatMessage.Action.USERS_ONLINE)) {
                        refreshOnlines(message);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(ClienteFrame.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ClienteFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void connected(ChatMessage message) {
        if (message.getText().equals("NO")) {
            nomePlayer = "";
            JOptionPane.showMessageDialog(null, "Conexão não reliazada!");
            return;
        }
        this.message = message;
        //JOptionPane.showMessageDialog(null, "Você está conectado no chat!");
    }

    public void disconnected() {
        ChatMessage message = new ChatMessage();
        message.setName(nomePlayer);
        message.setAction(Action.DISCONNECT);
        this.service.send(message);

    }

    private void receive(ChatMessage message) {
        //System.out.println("Entrou no receive");
        if (message.getText().contains("#")) {
            clienteChat.receive(message);
            return;
        }

        if (message.getText().contains("open")) {
            if (super.isAguardando() == false) {
                clienteChat = new ClienteFrame(this);
                clienteChat.setVisible(true);
                message.setSetOnlines(names);
                clienteChat.refreshOnlines(message);
                return;
            }
            
            super.setAguardando(false);            
            List<Point> posicoesJogadores = new ArrayList<>();
            List<Point> posicoesObstaculos = new ArrayList<>();
            List<Point> posicoes = ComandParse.toPositions(message.getText());
            for (int i = 0; i < posicoes.size(); i++) {
                if (i < MAX_PLAYERS)
                    posicoesJogadores.add(posicoes.get(i));
                else
                    posicoesObstaculos.add(posicoes.get(i));
            }
            carregarJogadores(posicoesJogadores);
            carregarElementos(posicoesObstaculos);
            
            clienteChat = new ClienteFrame(this);
            clienteChat.setVisible(true);
            message.setSetOnlines(names);
            clienteChat.refreshOnlines(message);
            return;
        }

        if (message.getText().equals("sair")) {
            disconnected();
            System.exit(0);
        }

        System.out.println(message.getName() + " entrou no jogo e deu o comando \" " + message.getText() + "\"");
        String comando = message.getText();

        if (comando.contains("atirar")) {
            int[] resps = ComandParse.toAttack(comando);
            int indiceJogador = resps[0];
            decidirAcao(indiceJogador, comando);
            return;
        }

        String[] acao = comando.split(",");
        //System.out.println(acao[0] + "" + acao[1]);
        decidirAcao(Integer.parseInt(acao[0]), acao[1]);
    }

    public void decidirAcao(int indice, String comando) {
        //System.out.println("tam: " + listaJogadores.size());
        Jogador jogadorAction = listaJogadores.get(indice);

        switch (comando) {

            /*case CIMA:
             jogador.moverCima(this, false);
             break;*/

            /*case BAIXO:
             jogador.moverBaixo(this, false);
             break;*/
            case DIREITA:
                listaJogadores.get(indice).moverDireita(this, false);
                break;

            case ESQUERDA:
                listaJogadores.get(indice).moverEsquerda(this, false);
                break;

            case DANO:
                if (indice == indiceUltimaJogada()) {
                    break;
                }
                listaJogadores.get(indice).sofreuDano(this, false, 10);
                listaJogadores.get(indice).getAnimator().setAnimation(SouthParkAnimator.HURTED);
                break;

            default:
                if (comando.contains(ATIRAR)) {
                    //System.out.println("entrou no atirar");
                    //parseia a string, pega x y e indice do jogador
                    int[] resps = ComandParse.toAttack(comando);
                    //int indiceJogadorAtual = resps[0];
                    int x = resps[1];
                    int y = resps[2];
                    projeteis.add(listaJogadores.get(indice).atirar(x, y, null));
                    proximoJogador();
                }
        }
    }

    public int indiceUltimaJogada() {
        return indiceJogadorAtual - 1 < 0 ? listaJogadores.size() - 1 : indiceJogadorAtual - 1;
    }

    public int getOnlines() {
        return this.onlines;
    }

    public void setOnlines(int onlines) {
        this.onlines = onlines;
    }

    private void refreshOnlines(ChatMessage message) {

        //listaJogadores = message.getJogadores();
        names = message.getSetOnlines();

        if (playerCounter == 0) {
            System.out.println("Player entrou!");
            playerCounter = names.size();
            //this.playerCounters
            //this.jogador.setIndice(names.size());
            //listaJogadores.set(this.jogador.getIndice() - 1, jogador);
            //JOptionPane.showMessageDialog(null, "Voce é o jogador de indice " + jogador.getIndice() + " número " + names.size());
        }
        setOnlines(names.size());
        //indiceJogadorAtual = getOnlines();
        System.out.println("Onlines: " + onlines);
        if (onlines == MAX_PLAYERS && abriu == false) {
            //System.out.println("Entru no if");
            super.setAguardando(false);
            abriu = true;
            
            SouthWormsLG lg = new SouthWormsLG(MAX_PLAYERS, 20, getWidth() - 100, 50);
            String msg = ComandParse.toLevel(lg.getPlayers(), lg.getObstacles());
            System.out.println(msg);
            enviarComando("open" + msg);
            
            carregarJogadores(lg.getPlayers());
            carregarElementos(lg.getObstacles());

            //super.run();
        }
        //String[] array = (String[]) names.toArray(new String[(names.size())]);
    }

    public void conectar() {
        if (nomePlayer != "") {
            this.message = new ChatMessage();
            this.message.setAction(ChatMessage.Action.CONNECT);
            this.message.setName(nomePlayer);
//            this.message.setJogador(jogador);

            this.service = new ClienteService();
            this.socket = this.service.connect();

            new Thread(new WormsGame.ListenerSocket(this.socket)).start();
            //System.out.println("Criou a thread");
            this.service.send(message);
            //listaJogadores.get(indiceJogadorAtual).setNome(nomePlayer);
        }
    }

    public void enviarComando(String comando) {
        System.out.println("comando recebido " + comando);
        //ENVIO DE COMANDOS AO SERVIDOR

        //String text = textAreaSend.getText();
        String name = this.message.getName();
        if (!comando.isEmpty()) {

            this.message = new ChatMessage();
            this.message.setName(name);
            this.message.setSetOnlines(names);
            this.message.setText(comando);
            this.message.setAction(ChatMessage.Action.SEND_ALL);

            this.service.send(this.message);

        }
    }

    @Override
    public void onLoad() {
        //carregarJogadores();
        //jogador = listaJogadores.get(1);
        abriu = false;

        tempoAtual = System.currentTimeMillis();
        janelaPrincipal.addMouseListener(this);
        janelaPrincipal.addKeyListener(this);
        keyPool = new HashMap<Integer, Boolean>();

        projeteis = new ArrayList<>();
        projeteis_colididos = new ArrayList<>();
        elementos_jogo = new ArrayList<>();
        elementos_colididos = new ArrayList<>();

        fase = new FaseA(this.janelaPrincipal);
    }

    public void carregarElementos(List<Point> posicoesObstaculos) {
        
        for (Integer indice : listaJogadores.keySet()) {
            elementos_jogo.add(listaJogadores.get(indice));
        }
        
        /*for (Point obstaculo : posicoesObstaculos) {
            //elementos_jogo.add();
        }*/        
        // obstaculos antes dos jogadores
        int x = 50;
        for (int i = 1; i <= 2; i++) {
            Obstaculo obs = new Obstaculo(x, 430, 45);
            elementos_jogo.add(obs);
            x += 100;
        }
        // obstaculos depois dos jogadores
        x = 520;
        for (int i = 1; i <= 2; i++) {
            Obstaculo obs = new Obstaculo(x, 430, 45);
            elementos_jogo.add(obs);
            x += 100;
        }
    }

    @Override
    public void onUnload() {
        
    }

    public void carregarJogadores(List<Point> posicoesJogadores) {   
        
        Object[] nms = names.toArray();
        int x = 230;
        for (int i = 1; i <= MAX_PLAYERS; i++) {
            Jogador player = new Jogador(listaPersonagens.getListaSkins().get(i - 1));
            player.setIsPlayer(true);
            player.setProjetilImage(listaPersonagens.getListaProjeteis().get(i - 1).getImage());
            player.setNome(nms[i - 1].toString());
            //player.setLifePlayer(30);
            player.setPos(x, player.getY1());
            //player.setPos(posicoesJogadores.get(i-1).x, player.getY1());
            x += 150;

            player.getAnimator().addAnimation(SouthParkAnimator.IDLE,
                    listaPersonagens.getListaSkins().get(i - 1).getImage());
            player.getAnimator().addAnimation(SouthParkAnimator.HIT,
                    listaPersonagens.getListaAtaques().get(i - 1).getImage());

            player.getAnimator().addAnimation(SouthParkAnimator.HURTED,
                    listaPersonagens.getListaHits().get(i - 1).getImage());

            player.setIndice(i - 1);
            listaJogadores.put(i - 1, player);
            System.out.println("nome do fdp: " + player.getNome());
        }
        txtMensagem = listaJogadores.get(0).getNome() + MSG_TURNO;
    }

    @Override
    public void onUpdate() {
        while (!listaJogadores.containsKey(indiceJogadorAtual)) {
            proximoJogador();
        }
        Jogador jogador = listaJogadores.get(indiceJogadorAtual);

        txtMensagem = jogador.getNome() + MSG_TURNO;
        tempoAtual = System.currentTimeMillis();

        // ****** Verifica colis�o dos proj�teis
        projeteis_colididos = new ArrayList<>();

        for (Projetil p_ : projeteis) {// percorre cada proj�til
            for (Elemento elemento : elementos_jogo) {
                // todos os elementos do jogo
                if (!p_.getColidiu()) {
                    // esse proj�til colidiu com algum elemento
                    // get elemento que colidiu
                    Elemento e = null;
                    e = p_.verificaColisao(elemento, p_, -1);
                    if (e != null) {
                        //System.out.println(e.getNome() + " Colidiu");
                        // lista de elementos colididos
                        elementos_colididos.add(e);
                    }
                }
                // se colidiu
                if (p_.getColidiu()) {
                    for (Elemento e : elementos_colididos) {
                        String nome = e.getNome();
                        System.out.println("####" + nome);
                        for (int i : listaJogadores.keySet()) {
                            Jogador player = listaJogadores.get(i);
                            if (player.getNome().equals(nome)) {
                                System.out.println("------------------------------- " + p_.getNome() + " bateu no " + nome);
                                decidirAcao(i, DANO);
                                break;
                            }
                        }
                    }
                    // retirar proj�til de cena
                    p_.setPos(-5000, -5000);
                    p_.setColidiu(false);
                    // coloca os proj�teis que colidiram na lista
                    projeteis_colididos.add(p_);
                }
            }
        }

        for (Projetil projetil : projeteis_colididos) {
            projeteis.remove(projetil);// remove os projeteis colididos do jogo
        }
        projeteis_colididos.clear();

        for (Elemento e : elementos_colididos) {
            if (!e.isPlayer()) {
                elementos_jogo.remove(e);// remove os elementos colididos com os
                e.setPos(-5000, -5000);
            }
        }
        elementos_colididos.clear();

        // ****** Verifica colis�o dos proj�teis
        if (indiceJogadorAtual == playerCounter - 1) {
            if (!listaJogadores.get(indiceJogadorAtual).getColidiu()) {
                // gc.getGraphics().setColor(Color.white);
                listaJogadores.get(indiceJogadorAtual).mover(getWidth(), getHeight(), keyPool, this); // capturar
                // movimenta��o
                // do
                // jogador
                // com
                // teclas
                //System.out.println("ind atual : " + indiceJogadorAtual + " indice jogador: " + jogador.getIndice());
                listaJogadores.get(indiceJogadorAtual).setPos(jogador.getX1(), jogador.getY1());// alterar a
            }
        }

        // ****** Verifica colis�o Jogador X Obstaculo
        // se jogador n�o colidiu
        if (!listaJogadores.get(indiceJogadorAtual).getColidiu()) {
            for (Elemento elemento : elementos_jogo) {
                //listaJogadores.get(indiceJogadorAtual).verificaColisao(elemento, jogador, jogador.getUltimoMovimento());
                if (listaJogadores.get(indiceJogadorAtual).verificaColisao(elemento, jogador, jogador.getUltimoMovimento()) != null) {
                    listaJogadores.get(indiceJogadorAtual).setColidiu(true);
                }
            }
        }

        if (listaJogadores.get(indiceJogadorAtual).getColidiu()) {
            // se colidiu
            // simular impacto com obstaculo
            listaJogadores.get(indiceJogadorAtual).setPos(jogador.getX_c(), jogador.getY_c());
            listaJogadores.get(indiceJogadorAtual).setColidiu(false);
        }

        // verifica quais jogadores morreram
        List<Integer> dead = new ArrayList<>();
        for (Map.Entry<Integer, Jogador> par : listaJogadores.entrySet()) {
            if (par.getValue().getLifePlayer() <= 0) {
                dead.add(par.getKey());
            }
        }
        // remove os mortos da lista
        for (int deadPlayer : dead) {
            listaJogadores.remove(deadPlayer);
        }

        if (listaJogadores.size() == 1) {
            int vencedor = 0;
            for (int v : listaJogadores.keySet()) {
                vencedor = v;
            }
            String nome = names.toArray()[vencedor].toString();
            txtMensagem = "Parabens Mano " + nome + " voce venceu _|_";
        }

        try {
            Thread.sleep(20);
        } catch (Exception e) {
            Logger.getLogger(WormsGame.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public int getIndiceJogadorAtual() {
        return indiceJogadorAtual;
    }

    public void setIndiceJogadorAtual(int indiceJogadorAtual) {
        this.indiceJogadorAtual = indiceJogadorAtual;
    }

    public ChatMessage getMessage() {
        return this.message;
    }

    @Override
    public void onRender(Graphics2D g) {

        fase.onRender(g);
        g.setFont(new Font("default", Font.BOLD, 20));
        g.setColor(Color.RED);
        g.drawString(txtMensagem, 50, 50);
        g.setColor(Color.BLACK);
        
        for (Elemento elemento : elementos_jogo) {
            if (elemento.isPlayer())
                continue;
            elemento.onRender(g);
        }

        for (Map.Entry<Integer, Jogador> par : listaJogadores.entrySet()) {
            par.getValue().onRender(g);
        }

        for (Projetil p : projeteis) {
            p.onRender(g, FORCE);
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        keyPool.put(e.getKeyCode(), true);
    }

    @Override
    public void keyReleased(KeyEvent e) {

        keyPool.remove(e.getKeyCode());

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent arg0) {
    }

    @Override
    public void windowClosed(WindowEvent arg0) {
    }

    @Override
    public void windowDeactivated(WindowEvent arg0) {
    }

    @Override
    public void windowDeiconified(WindowEvent arg0) {
    }

    @Override
    public void windowIconified(WindowEvent arg0) {
    }

    @Override
    public void windowOpened(WindowEvent arg0) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (indiceJogadorAtual == playerCounter - 1) {
            listaJogadores.get(indiceJogadorAtual).getAnimator().setAnimation(SouthParkAnimator.HIT);
            projeteis.add(listaJogadores.get(indiceJogadorAtual).atirar(e.getX(), e.getY(), this));
            proximoJogador();
        }
    }

    public void proximoJogador() {
        indiceJogadorAtual++;
        if (indiceJogadorAtual >= MAX_PLAYERS) {
            indiceJogadorAtual = 0;
        }
        /*if (indiceJogadorAtual == playerCounter - 1) {
         //JOptionPane.showMessageDialog(janelaPrincipal, "Agora é a sua vez de jogar!");
         txtMensagem = "Sua vez de jogar!";
         } else {
         //JOptionPane.showMessageDialog(janelaPrincipal, "Agora é a vez do jogador " + indiceJogadorAtual);
         txtMensagem = "Segura ae fera!";
         }*/
    }

    public long getTempoAntes() {
        return tempoAntes;
    }

    public void setTempoAntes(long tempoAntes) {
        this.tempoAntes = tempoAntes;
    }

    public long getTempoAtual() {
        return tempoAtual;
    }

    public void setTempoAtual(long tempoAtual) {
        this.tempoAtual = tempoAtual;
    }

    public void attTempo() {
        tempoAntes = System.currentTimeMillis();
    }

    public long checaTempo() {
        return tempoAtual - tempoAntes;
    }

    public String getNomePlayer() {
        return nomePlayer;
    }

    public void setNomePlayer(String nomePlayer) {
        this.nomePlayer = nomePlayer;
    }

}
