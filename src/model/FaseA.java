package model;

//import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.nio.file.Path;
import java.nio.file.Paths;
//import java.awt.Rectangle;
import javax.swing.ImageIcon;

public class FaseA extends Elemento {

	// private Rectangle platform;
    // private int limiteW, limiteH;
    private Image sceneImage, soloImage;

    public FaseA(javax.swing.JFrame frame) {

        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        ImageIcon referencia = new ImageIcon(s + "\\images\\KennyAnimations\\Cenario.png");

//        ImageIcon referencia = new ImageIcon(getClass().getClassLoader()
//                .getResource("images\\KennyAnimations\\Cenario.png"));
        currentRelativePath = Paths.get("");
        s = currentRelativePath.toAbsolutePath().toString();
        ImageIcon soloFase = new ImageIcon(s + "\\images\\KennyAnimations\\soloCenario.png");

//        ImageIcon soloFase = new ImageIcon(getClass().getClassLoader()
//                .getResource("images\\KennyAnimations\\soloCenario.png"));
        sceneImage = referencia.getImage();
        soloImage = soloFase.getImage();

        nome = "";
		// limiteH = frame.getHeight();
        // limiteW = frame.getWidth();
        // platform = new Rectangle(0, 450, limiteW, limiteH);
    }

    public void onRender(Graphics2D g) {
		// g.setColor(Color.WHITE);
        // g.fillRect(platform.x, platform.y, limiteW, limiteH);
        g.drawImage(sceneImage, 0, 0, null);
        g.drawImage(soloImage, 0, 0, null);
        //g.drawString(WormsGame.txtMensagem, 10, 10);
    }

    /*
     * public void limitadorTela(int w, int h) { limiteW = w; limiteH = h; }
     */

    /*
     * public boolean houveColisao(Rectangle entity) { return
     * platform.intersects(entity); }
     */
}
