package model;

import animator.Animator;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Map;

public abstract class Elemento {

    public static final int DIREITA = 1;
    public static final int ESQUERDA = 2;
    public static final int CIMA = 3;
    public static final int BAIXO = 4;
    static final int FORCE = 10;

    
    protected Animator animator;
    protected Image elementoImage;
    private boolean isPlayer;
    private int ultimoMovimento;

    protected String nome = "";

	// x1 e y1 = coordenadas do canto superior esquerdo do retangulo
    // x2 e y2 = coordenadas do canto inferior direito do retangulo
    protected int x1, y1, x2, y2, largura, altura;
    private boolean colidiu = false;
    // x_c\y_c coordenadas para sair da colis�o
    int x_c = x1, y_c = y1;

    // verifica se houve colis�o
    public boolean colidiu(Elemento obstaculo) {
        Rectangle rec1 = new Rectangle(x1, y1, 50, 50);
        Rectangle rec2 = new Rectangle(obstaculo.x1,obstaculo.y1,50,50);
        return rec1.intersects(rec2);
        /*
	// canto inferior direito (jogador) com o superior esquerdo (obstaculo)
        // est�o distantes
        // pra x
        if (y2 < obstaculo.getY1()) {
            return false;
        }
        // pra y
        if (x2 < obstaculo.getX1()) {
            return false;
        }

		// canto superior esquerdo (obstaculo) com inferior direito (jogador)
        // est�o distantes
        // pra x
        if (y1 > obstaculo.getY2()) {
            return false;
        }
        // pra y
        if (x1 > obstaculo.getX2()) {
            //System.out.println("Entrou no 4 if");
            return false;
        }

        return true;*/
    }

    /**
     * *
     * Este m�todo � aplicado ap�s contatada a colis�o, ele � respons�vel por
     * tirar o objeto da colis�o impulsionando-o para a dire��o oposta ao
     * impacto
     *
     * @param jogador_ objeto colidido
     * @param ultimoMovimento ultimo movimento do jogador
     * @return colisoes, um mapa contendo as posi��es utilizadas para tirar o
     * jogador da colis�o com o obstaculo
     */
    public Map posicoes_sair_colisao(Elemento jogador_, int ultimoMovimento) {

        Map<String, Integer> colisoes = new HashMap<String, Integer>();

        if (ultimoMovimento != -1) {

            int x_, y_;

            // trombou em baixo do obstaculo
            if (ultimoMovimento == CIMA) {
                if (y2 >= jogador_.getY1()) {
                    y_ = jogador_.getY1() + (y2 - jogador_.getY1());
                    colisoes.put("y", y_); // posi��o para sair da colis�o
                    colisoes.put("y_c", y_ + FORCE);// posi��o para simular um
                    // impacto

                }
            }

            if (ultimoMovimento == ESQUERDA) {
                if (x2 >= jogador_.getX1()) {
                    x_ = jogador_.getX1() + (x2 - jogador_.getX1());
                    colisoes.put("x", x_);
                    colisoes.put("x_c", x_ + FORCE);

                }
            }

            if (ultimoMovimento == BAIXO) {
                if (y1 <= jogador_.getY2()) {
                    y_ = jogador_.getY1() - (jogador_.getY2() - y1);
                    colisoes.put("y", y_);
                    colisoes.put("y_c", y_ - FORCE);
                }
            }

            if (ultimoMovimento == DIREITA) {
                if (x1 <= jogador_.getX2()) {
                    x_ = jogador_.getX1() - (jogador_.getX2() - x1);
                    colisoes.put("x", x_);
                    colisoes.put("x_c", x_ - FORCE);
                }
            }
        } else {
            System.out.println("Colidiu");

        }
        return colisoes;
    }

    public Elemento verificaColisao(Elemento b1, Elemento b2, int ultimoMovimento) {
        if(b1.isPlayer && b2.isPlayer){
            return null;
        }        
        //System.out.println(b1.getX1() + " , " + b1.getY1() + " nome: " + b1.getNome());
        //System.out.println(b2.getX1() + " , " + b2.getY1() + " nome: " + b2.getNome());
        
        if (b1.colidiu(b2)) {
            colidiu = true;
            x_c = x1;
            y_c = y1;

            Map<String, Integer> colisoes = b1.posicoes_sair_colisao(b2, ultimoMovimento);

            if (colisoes.containsKey("x")) {
                x1 = colisoes.get("x");
                x_c = colisoes.get("x_c");
            }

            if (colisoes.containsKey("y")) {
                y1 = colisoes.get("y");
                y_c = colisoes.get("y_c");
            }
            return b1;

        }
        return null;

    }

    public void setPos(int x, int y) {
        x1 = x;
        y1 = y;
        x2 = x + largura;
        y2 = y + altura;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public void setLargura(int largura) {
        this.largura = largura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public Elemento() {
        nome = "";
        x1 = y1 = largura = altura = 0;
        isPlayer = false;
    }

    public void onRender(Graphics2D g) {
    }

    public boolean isPlayer() {
        return isPlayer;
    }

    public void setIsPlayer(boolean isPlayer) {
        this.isPlayer = isPlayer;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getX1() {
        return x1;
    }

    public void setX1(int x) {
        this.x1 = x;
    }

    public int getY1() {
        return y1;
    }

    public void setY1(int y) {
        this.y1 = y;
    }

    public int getLargura() {
        return largura;
    }

    public int getAltura() {
        return altura;
    }

    public int getUltimoMovimento() {
        return ultimoMovimento;
    }

    public void setUltimoMovimento(int ultimoMovimento) {
        this.ultimoMovimento = ultimoMovimento;
    }

    public static int getForce() {
        return FORCE;
    }

    public int getX_c() {
        return x_c;
    }

    public void setX_c(int x_c) {
        this.x_c = x_c;
    }

    public int getY_c() {
        return y_c;
    }

    public void setY_c(int y_c) {
        this.y_c = y_c;
    }

    public boolean getColidiu() {
        return colidiu;
    }

    public void setColidiu(boolean c) {
        colidiu = c;
    }
    
    public Image getElementoImage() {
        return elementoImage;
    }

    public void setElementoImage(Image playerImage) {
        this.elementoImage = playerImage;
    }
}
