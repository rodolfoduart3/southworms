/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package level.generation;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Luis
 */
public class SouthWormsLG {
    
    public int MaxPlayers, MaxObstacles, MinWidth, MaxWidth, Space;
    public static final Random PRNG = new Random();
    private List<Point> Players, Obstacles;

    public List<Point> getPlayers() {
        return Players;
    }

    public List<Point> getObstacles() {
        return Obstacles;
    }

    public SouthWormsLG(int MaxPlayers, int MinWidth, int MaxWidth, int Space) {
        System.out.println("gerando");
        this.MaxPlayers = MaxPlayers;
        this.MaxObstacles = MaxPlayers * 2;
        this.MinWidth = MinWidth;
        this.MaxWidth = MaxWidth;
        this.Space = Space;
        this.GenerateLevel();
    }
    
    public void GenerateLevel(){
        
        this.Players = new ArrayList<>();
        this.Obstacles = new ArrayList<>();
        
        while(this.Players.size() < MaxPlayers){
            
            int x = MinWidth + PRNG.nextInt(MaxWidth - MinWidth);
            int y = 0;
            
            boolean exist = false;
            for (Point player : this.Players) {
                if (Math.abs(player.getX() - x) <= Space)
                {
                    exist = true;
                    break;
                }
            }            
            if (exist)
                continue;
            
            this.Players.add(new Point(x, y));
        }
        
        while(this.Obstacles.size() < MaxObstacles){
            
            int x = MinWidth + PRNG.nextInt(MaxWidth - MinWidth);
            int y = 0;
            
            boolean exist = false;
            for (Point player : this.Players) {
                if (Math.abs(player.getX() - x) <= Space)
                {
                    exist = true;
                    break;
                }
            }            
            if (exist)
                continue;
            
            this.Obstacles.add(new Point(x, y));
        }
    }
}
