package animator;

import java.awt.Image;
import model.Elemento;

import model.Jogador;

public class SouthParkAnimator extends Animator {

	private static final float TIME_LIMIT = 1f;
	public static final String IDLE = "idle";
	public static final String HIT = "hit";
	public static final String HURTED = "hurted";
	
	public SouthParkAnimator(Elemento playerRef) {
		super(playerRef);
	}

	@Override
	public void update(){
		double tempo = this.timer.parar() / 1000;
		//System.out.println("ESTE � O TEMPO = " + tempo);
		if (tempo >= TIME_LIMIT){
			this.elementoRef.setElementoImage(this.animations.get(IDLE));
			this.timer.zerar();
		}
	}

	public void setAnimation(String animationId){
		if (!this.animations.containsKey(animationId))
			return;

		Image im = this.animations.get(animationId);
		this.elementoRef.setElementoImage(im);
		this.timer.iniciar();
	}
}