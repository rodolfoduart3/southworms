package animator;

public class Cronometro {
 
    public Cronometro() {
    }
    
    /**
     * Inicia ou reinicia a contagem de tempo. Nunca zera o último estado do contador. Se o tempo já
     * estiver correndo, não faz nada.
     */
    public void iniciar() {
        inicio = System.currentTimeMillis();
    }
    
    /**
     * Para a contagem de tempo e retorna o tempo decorrido até o momento da parada.
     * 
     * @return Tempo decorrido até o momento da parada.
     */
    public double parar() {
        fim = System.currentTimeMillis();
        return fim-inicio;
    }
    
    /**
     * Retorna o tempo decorrido contado até então, independente se está parado ou correndo. Não
     * altera o estado de contagem (parado/correndo).
     * 
     * return Tempo decorrido contado pelo cronômetro.
     */
    public double lerTempoEmMilissegundos() {
        return fim;
    }
    
    /**
     * Zera o contador de tempo do cronômetro. Se o cronômetro estava em estado de contagem, ele é
     * parado.
     */
    public void zerar() {
        inicio = 0;
    }
    
    private double inicio = System.currentTimeMillis();
    private double fim;
}
