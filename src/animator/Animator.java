package animator;

import java.awt.Image;
import java.util.HashMap;
import java.util.Map;
import model.Elemento;

import model.Jogador;

public abstract class Animator {
	
	protected Elemento elementoRef;
	protected Map<String, Image> animations;
	protected Cronometro timer;

	public Animator(Elemento playerRef){
		animations = new HashMap<String, Image>();
		timer = new Cronometro();
		this.elementoRef = playerRef;
	}

	public void addAnimation(String id, Image image){
		if (this.animations.containsKey(id))
			return;
		this.animations.put(id, image);
	}

	
	public abstract void update();
	public abstract void setAnimation(String animationId);

}
