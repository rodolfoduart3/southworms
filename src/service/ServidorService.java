/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import bean.ChatMessage;
import bean.ChatMessage.Action;
import java.util.ArrayList;
import java.util.List;
import model.Jogador;

/**
 *
 * @author paulo.bueno
 */
public class ServidorService {

    private ServerSocket serverSocket;
    private Socket socket;
    private Map<String, ObjectOutputStream> mapOnlines = new HashMap<String, ObjectOutputStream>();
    private List<Jogador> jogadores = new ArrayList<>();

    public ServidorService() {
        try {
            serverSocket = new ServerSocket(5555);
            JOptionPane.showMessageDialog(null, "Servidor aberto!");

            while (true) {
                socket = serverSocket.accept();
                new Thread(new ListenerSocket(socket)).start();
            }

        } catch (IOException ex) {
            Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private class ListenerSocket implements Runnable {

        private ObjectOutputStream output;
        private ObjectInputStream input;

        public ListenerSocket(Socket socket) {
            try {
                this.output = new ObjectOutputStream(socket.getOutputStream());
                this.input = new ObjectInputStream(socket.getInputStream());
            } catch (IOException ex) {
                Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        public void run() {
            ChatMessage message = null;

            try {
                while ((message = (ChatMessage) input.readObject()) != null) {
                    Action action = message.getAction();

                    if (action.equals(Action.CONNECT)) {
                        boolean isConnected = connect(message, output);
                        if (isConnected) {
                            mapOnlines.put(message.getName(), output);
                            //jogadores.add(message.getJogador());
                            JOptionPane.showMessageDialog(null, "Jogador " + message.getName() + " Conectado!");
                            System.out.println("Jogadores conectados: " + mapOnlines.size());
                            sendOnlines();
                        }
                    } else if (action.equals(Action.DISCONNECT)) {
                        disconnect(message);
                        return;
                    } else if (action.equals(Action.SEND_ONE)) {
                        sendOne(message, output);
                    } else if (action.equals(Action.SEND_ALL)) {
                        sendAll(message);
                    } else if (action.equals(Action.USERS_ONLINE)) {

                    }
                }
            } catch (IOException ex) {
                ChatMessage cm = new ChatMessage();
                System.out.println("getname do message: " + message.getName());
                cm.setName(message.getName());
                disconnect(cm);
                sendOnlines();
                System.out.println(message.getName() + " deixou o chat!");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        private boolean connect(ChatMessage message, ObjectOutputStream output) {
            if (mapOnlines.size() == 0) {
                message.setText("YES");
                sendOne(message, output);

                return true;
            }

            /*for (Map.Entry<String, ObjectOutputStream> kv : mapOnlines.entrySet()) {
             if (kv.getKey().equals(message.getName())) {
             message.setText("NO");
             sendOne(message, output);
             return false;
             } else {
             message.setText("YES");
             sendOne(message, output);
             return true;
             }
             }*/
            if (mapOnlines.containsKey(message.getName())) {
                message.setText("NO");
                sendOne(message, output);
                return false;
            } else {
                message.setText("YES");
                sendOne(message, output);
                return true;
            }

        }

        private void disconnect(ChatMessage message) {
            mapOnlines.remove(message.getName());
            jogadores.remove(message.getJogador());

            message.setText("sair");

            message.setAction(Action.SEND_ONE);

            sendAll(message);

            System.out.println("Usuário " + message.getName() + " saiu");
        }

        private void sendOne(ChatMessage message, ObjectOutputStream output) {
            try {
                output.writeObject(message);
            } catch (IOException ex) {
                Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        private void sendAll(ChatMessage message) {
            for (Map.Entry<String, ObjectOutputStream> kv : mapOnlines.entrySet()) {
                if (!kv.getKey().equals(message.getName())) {
                    message.setAction(Action.SEND_ONE);
                    try {
                        kv.getValue().writeObject(message);
                    } catch (IOException ex) {
                        Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }

        private void sendOnlines() {
            Set<String> setNames = new HashSet<String>();

            for (Map.Entry<String, ObjectOutputStream> kv : mapOnlines.entrySet()) {
                setNames.add(kv.getKey());
            }

            ChatMessage message = new ChatMessage();
            message.setAction(Action.USERS_ONLINE);
            message.setSetOnlines(setNames);

            for (Map.Entry<String, ObjectOutputStream> kv : mapOnlines.entrySet()) {
                try {
                    kv.getValue().writeObject(message);
                } catch (IOException ex) {
                    Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }

    }

}
