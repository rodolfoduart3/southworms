package core;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferStrategy;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;

abstract public class Game implements WindowListener {

    protected JFrame janelaPrincipal;
    private boolean executando, aguardando;

    protected BufferStrategy bufferStrategy;
    protected int teclaPressionada;

    public Game() {
        janelaPrincipal = new JFrame("Worms - Sockets");
        janelaPrincipal.setSize(new Dimension(800, 500));
        janelaPrincipal.addWindowListener((WindowListener) this);
        janelaPrincipal.setLocation(20,20);
        janelaPrincipal.setResizable(false);
        janelaPrincipal.setDefaultCloseOperation(janelaPrincipal.EXIT_ON_CLOSE);
        executando = false;
    }

    public void update() {
        onUpdate();
        Thread.yield();
    }

    public void run() {
        executando = true;
        aguardando = true;
      
        load();

        while (executando) {
            try {
                //System.out.println(" ?? " + aguardando);
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (aguardando == false) {

                update();
                render();
            }
        }
        unload();
    }

    public void render() {
        Graphics2D g = (Graphics2D) bufferStrategy.getDrawGraphics();
        g.setColor(Color.black);
        g.fillRect(0, 0, janelaPrincipal.getWidth(), janelaPrincipal.getHeight());
        onRender(g);
        g.dispose();
        bufferStrategy.show();
    }

    public void load() {
        janelaPrincipal.setIgnoreRepaint(true);
        janelaPrincipal.setLocation(20, 20);
        janelaPrincipal.setVisible(true);
        janelaPrincipal.createBufferStrategy(2);
        bufferStrategy = janelaPrincipal.getBufferStrategy();
        onLoad();
    }

    public void unload() {
        onUnload();
        bufferStrategy.dispose();
        janelaPrincipal.dispose();
    }

    public void terminate() {
        executando = false;
    }

    public void windowClosing(WindowEvent e) {
        terminate();
    }

    public void setExecutando(boolean executando) {
        this.executando = executando;
    }

    public boolean isAguardando() {
        return aguardando;
    }

    public void setAguardando(boolean aguardando) {
        this.aguardando = aguardando;
    }

    public boolean estaExecutando() {
        return this.executando;
    }

    public int getWidth() {
        return janelaPrincipal.getWidth();
    }

    public int getHeight() {
        return janelaPrincipal.getHeight();
    }

    abstract public void onLoad();

    abstract public void onUnload();

    abstract public void onUpdate();

    abstract public void onRender(Graphics2D g);

}
