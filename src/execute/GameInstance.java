/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package execute;

import model.WormsGame;

/**
 *
 * @author Paulo
 */
public class GameInstance {
    
    private GameInstance() {
    }
    
    public static WormsGame getInstance() {
        return GameInstanceHolder.INSTANCE;
    }
    
    private static class GameInstanceHolder {

        private static final WormsGame INSTANCE = new WormsGame();
    }
}
